#!/usr/bin/env bash

function obj() {
    NAME="Devbox"
    DESCRIPTION=""
}

function install() {
    echo "############ INSTALLING DEVBOX ############"

    sudo apt install curl -y &&

    curl -fsSL https://get.jetify.com/devbox | bash

    echo "############ DONE DEVBOX ############"
}
