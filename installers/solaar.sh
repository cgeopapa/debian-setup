#!/usr/bin/env bash

function obj() {
    NAME="Solaar"
    DESCRIPTION="Logitec device manager"
}

function install() {
    echo "############ INSTALLING SOLAAR ############"

    sudo apt install solaar -y

    echo "############ DONE SOLAAR ############"
}
