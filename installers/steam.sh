#!/usr/bin/env bash

function obj() {
    NAME="Steam"
    DESCRIPTION=""
}

function install() {
    echo "############ INSTALLING STEAM ############"

    sudo apt install wget -y &&

    wget https://cdn.cloudflare.steamstatic.com/client/installer/steam.deb &&

    sudo apt install ./steam.deb -y

    echo "############ DONE STEAM ############"
}
