#!/usr/bin/env bash

function obj() {
    NAME="Jetbrains Toolbox"
    DESCRIPTION=""
}

function install() {
    echo "############ INSTALLING TOOLBOX ############"

    sudo apt install curl wget -y &&

    curl -fsSL https://raw.githubusercontent.com/nagygergo/jetbrains-toolbox-install/master/jetbrains-toolbox.sh | bash

    echo "############ DONE TOOLBOX ############"
}
