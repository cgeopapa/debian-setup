#!/usr/bin/env bash

function obj() {
    NAME="Git"
    DESCRIPTION="Will also configure username and email"
}

function install() {
    echo "############ INSTALLING GIT ############"

    sudo apt install git -y &&

    git config --global user.name "cgeopapa" && 
    git config --global user.email "cgeocodgod@gmail.com"

    echo "############ DONE GIT ############"
}
