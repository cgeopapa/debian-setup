#!/usr/bin/env bash

function obj() {
    NAME="Grub"
    DESCRIPTION="Disable Grub menu from showing"
}

function install() {
    echo "############ DISABLING GRUB ############"

    if grep -q "GRUB_TIMEOUT" "/etc/default/grub"; then
        # If the string exists, delete the line containing it using sed
        sudo sed -i "/GRUB_TIMEOUT/d" "/etc/default/grub"
    fi

    echo "GRUB_TIMEOUT=0" | sudo tee -a /etc/default/grub > /dev/null

    sudo update-grub

    echo "############ DONE GRUB ############"
}
