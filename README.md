# debian-setup

A script that let's you select the software you want installed in your fresh Debian installation.

## Current installers
- devbox
- firefox
- git
- grub (disable grub menu)
- solaar
- steam
- thunderbird
- jetbrains toolbox
- vs code

## To be added
- rsync
- spotify

### Check if possible
- rsync setup (GDrive)
- ssh keys
- toolbox account connect
- webstorm with toolbox