#!/usr/bin/env bash

sudo apt update &&
sudo apt install whiptail tr -y

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd /tmp && clear

cd $SCRIPT_DIR/installers
sudo chmod +x ./*.sh
WHIPTAIL=()
declare -A INSTALLERS
for INSTALLER in $(ls *.sh); do
    source ./$INSTALLER
    obj
    INSTALLERS[$NAME]="$(pwd)/$INSTALLER"
    WHIPTAIL+=("${NAME}" "${DESCRIPTION}" OFF)
done

eval CHOICES=($(whiptail --title "INSTALL" --checklist \
"Choose what to isntall" 20 70 10 \
"${WHIPTAIL[@]}" 3>&1 1>&2 2>&3))

for CHOICE in "${CHOICES[@]}"; do
    source ${INSTALLERS[$CHOICE]}
    install
done
